package com.l2jfrozen.database.model.game;

/**
 * Enumeration of locations for item.
 */
public enum ItemLocation
{
    /** The VOID. */
    VOID,

    /** The INVENTORY. */
    INVENTORY,

    /** The PAPERDOLL. */
    PAPERDOLL,

    /** The WAREHOUSE. */
    WAREHOUSE,

    /** The CLANWH. */
    CLANWH,

    /** The PET. */
    PET,

    /** The PE t_ equip. */
    PET_EQUIP,

    /** The LEASE. */
    LEASE,

    /** The FREIGHT. */
    FREIGHT
}
