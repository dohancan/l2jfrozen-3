package com.l2jfrozen.database;

import com.l2jfrozen.configuration.ConfigManager;
import com.l2jfrozen.configuration.DatabaseConfig;
import org.junit.BeforeClass;

/**
 * User: vdidenko
 * Date: 28.11.13
 * Time: 7:37
 */
public class BaseTest {
    private static DatabaseConfig databaseConfig;

    @BeforeClass
    public static void setUpClass() throws Exception {

    }

    protected static DatabaseConfig getDatabaseConfig() {
        return databaseConfig;
    }
}
