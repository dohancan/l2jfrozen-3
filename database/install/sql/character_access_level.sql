/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50520
Source Host           : localhost:3306
Source Database       : gameserver_beta

Target Server Type    : MYSQL
Target Server Version : 50520
File Encoding         : 65001

Date: 2014-02-25 23:22:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for character_access_level
-- ----------------------------
CREATE TABLE `character_access_level` (
  `_id` decimal(10,0) NOT NULL,
  `access_level` int(11) DEFAULT NULL,
  `allowAltg` tinyint(1) DEFAULT NULL,
  `allowFixedRes` tinyint(1) DEFAULT NULL,
  `allowPeaceAttack` tinyint(1) DEFAULT NULL,
  `allowTransaction` tinyint(1) DEFAULT NULL,
  `canDisableGmStatus` tinyint(1) DEFAULT NULL,
  `gainExp` tinyint(1) DEFAULT NULL,
  `giveDamage` tinyint(1) DEFAULT NULL,
  `isGm` tinyint(1) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `nameColor` varchar(255) DEFAULT NULL,
  `takeAggro` tinyint(1) DEFAULT NULL,
  `titleColor` varchar(255) DEFAULT NULL,
  `use_name_color` tinyint(1) DEFAULT NULL,
  `useTitleColor` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id` (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of character_access_level
-- ----------------------------
INSERT INTO `character_access_level` VALUES ('0', '0', '0', '0', '0', '1', '0', '1', '1', '0', 'Default', 'FFFFFF', '0', 'FFFFFF', '1', '1');
INSERT INTO `character_access_level` VALUES ('1', '1', '1', '1', '1', '1', '1', '1', '1', '1', 'Master Access', '0099FF', '1', '0099FF', '1', '1');
INSERT INTO `character_access_level` VALUES ('2', '2', '1', '1', '1', '1', '1', '1', '1', '1', 'Head GM', '00FFFF', '1', '00FFFF', '1', '0');
INSERT INTO `character_access_level` VALUES ('3', '3', '1', '1', '1', '0', '0', '0', '0', '1', 'Event GM', '00FFFF', '0', '00FFFF', '1', '0');
INSERT INTO `character_access_level` VALUES ('4', '4', '1', '1', '0', '0', '0', '0', '0', '1', 'Support GM', '00FFFF', '0', '00FFFF', '1', '0');
INSERT INTO `character_access_level` VALUES ('5', '5', '1', '1', '0', '0', '0', '0', '0', '1', 'General GM', '00FFFF', '0', '00FFFF', '1', '0');
INSERT INTO `character_access_level` VALUES ('6', '6', '1', '1', '0', '0', '0', '0', '0', '0', 'Test GM', 'FFFFFF', '0', 'FFFFFF', '1', '0');

-- ----------------------------
-- Table structure for character_admin_command_access_right
-- ----------------------------
CREATE TABLE `character_admin_command_access_right` (
  `_id` decimal(10,0) NOT NULL,
  `admin_command` varchar(64) DEFAULT NULL,
  `access_level` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`_id`),
  UNIQUE KEY `_id` (`_id`),
  KEY `FK714D95BB840984C0` (`access_level`),
  CONSTRAINT `FK714D95BB840984C0` FOREIGN KEY (`access_level`) REFERENCES `character_access_level` (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of character_admin_command_access_right
-- ----------------------------
INSERT INTO `character_admin_command_access_right` VALUES ('1', 'admin_admin', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('2', 'admin_admin1', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('3', 'admin_admin2', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('4', 'admin_admin3', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('5', 'admin_admin4', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('6', 'admin_admin5', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('7', 'admin_gmliston', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('8', 'admin_gmlistoff', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('9', 'admin_silence', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('10', 'admin_diet', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('11', 'admin_set', '1');
INSERT INTO `character_admin_command_access_right` VALUES ('12', 'admin_set_menu', '1');
INSERT INTO `character_admin_command_access_right` VALUES ('13', 'admin_set_mod', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('14', 'admin_saveolymp', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('15', 'admin_manualhero', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('16', 'admin_list_announcements', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('17', 'admin_reload_announcements', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('18', 'admin_announce_announcements', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('19', 'admin_add_announcement', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('20', 'admin_del_announcement', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('21', 'admin_announce', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('22', 'admin_critannounce', '1');
INSERT INTO `character_admin_command_access_right` VALUES ('23', 'admin_announce_menu', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('24', 'admin_list_autoannouncements', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('25', 'admin_add_autoannouncement', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('26', 'admin_del_autoannouncement', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('27', 'admin_autoannounce', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('28', 'admin_ban', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('29', 'admin_unban', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('30', 'admin_jail', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('31', 'admin_unjail', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('32', 'admin_banchat', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('33', 'admin_unbanchat', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('34', 'admin_bbs', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('35', 'admin_boat', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('36', 'admin_getbuffs', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('37', 'admin_stopbuff', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('38', 'admin_stopallbuffs', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('39', 'admin_areacancel', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('40', 'admin_cache_htm_rebuild', '1');
INSERT INTO `character_admin_command_access_right` VALUES ('41', 'admin_cache_htm_reload', '1');
INSERT INTO `character_admin_command_access_right` VALUES ('42', 'admin_cache_reload_path', '1');
INSERT INTO `character_admin_command_access_right` VALUES ('43', 'admin_cache_reload_file', '1');
INSERT INTO `character_admin_command_access_right` VALUES ('44', 'admin_cache_crest_rebuild', '1');
INSERT INTO `character_admin_command_access_right` VALUES ('45', 'admin_cache_crest_reload', '1');
INSERT INTO `character_admin_command_access_right` VALUES ('46', 'admin_cache_crest_fix', '1');
INSERT INTO `character_admin_command_access_right` VALUES ('47', 'admin_changelvl', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('48', 'admin_christmas_start', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('49', 'admin_christmas_end', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('50', 'admin_itemcreate', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('51', 'admin_create_item', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('52', 'admin_ctf', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('53', 'admin_ctf_name', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('54', 'admin_ctf_desc', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('55', 'admin_ctf_join_loc', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('56', 'admin_ctf_edit', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('57', 'admin_ctf_control', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('58', 'admin_ctf_minlvl', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('59', 'admin_ctf_maxlvl', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('60', 'admin_ctf_tele_npc', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('61', 'admin_ctf_tele_team', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('62', 'admin_ctf_tele_flag', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('63', 'admin_ctf_npc', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('64', 'admin_ctf_npc_pos', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('65', 'admin_ctf_reward', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('66', 'admin_ctf_reward_amount', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('67', 'admin_ctf_team_add', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('68', 'admin_ctf_team_remove', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('69', 'admin_ctf_team_pos', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('70', 'admin_ctf_team_color', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('71', 'admin_ctf_team_flag', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('72', 'admin_ctf_join', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('73', 'admin_ctf_teleport', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('74', 'admin_ctf_start', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('75', 'admin_ctf_abort', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('76', 'admin_ctf_finish', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('77', 'admin_ctf_sit', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('78', 'admin_ctf_dump', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('79', 'admin_ctf_save', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('80', 'admin_ctf_load', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('81', 'admin_ctf_jointime', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('82', 'admin_ctf_eventtime', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('83', 'admin_ctf_autoevent', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('84', 'admin_ctf_minplayers', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('85', 'admin_ctf_maxplayers', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('86', 'admin_ctf_interval', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('87', 'admin_cw_info', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('88', 'admin_cw_remove', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('89', 'admin_cw_goto', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('90', 'admin_cw_reload', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('91', 'admin_cw_add', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('92', 'admin_cw_info_menu', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('93', 'admin_delete', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('94', 'admin_character_disconnect', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('95', 'admin_dmevent', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('96', 'admin_dmevent_name', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('97', 'admin_dmevent_desc', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('98', 'admin_dmevent_join_loc', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('99', 'admin_dmevent_minlvl', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('100', 'admin_dmevent_maxlvl', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('101', 'admin_dmevent_npc', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('102', 'admin_dmevent_npc_pos', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('103', 'admin_dmevent_reward', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('104', 'admin_dmevent_reward_amount', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('105', 'admin_dmevent_spawnpos', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('106', 'admin_dmevent_color', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('107', 'admin_dmevent_join', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('108', 'admin_dmevent_teleport', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('109', 'admin_dmevent_start', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('110', 'admin_dmevent_abort', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('111', 'admin_dmevent_finish', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('112', 'admin_dmevent_sit', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('113', 'admin_dmevent_dump', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('114', 'admin_dmevent_save', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('115', 'admin_dmevent_load', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('116', 'admin_setdonator', '1');
INSERT INTO `character_admin_command_access_right` VALUES ('117', 'admin_open', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('118', 'admin_close', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('119', 'admin_openall', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('120', 'admin_closeall', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('121', 'admin_changename', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('122', 'admin_edit_character', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('123', 'admin_current_player', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('124', 'admin_nokarma', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('125', 'admin_setkarma', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('126', 'admin_character_list', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('127', 'admin_character_info', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('128', 'admin_show_characters', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('129', 'admin_find_character', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('130', 'admin_find_dualbox', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('131', 'admin_find_ip', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('132', 'admin_find_account', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('133', 'admin_save_modifications', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('134', 'admin_rec', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('135', 'admin_setclass', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('136', 'admin_settitle', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('137', 'admin_setsex', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('138', 'admin_setcolor', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('139', 'admin_fullfood', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('140', 'admin_remclanwait', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('141', 'admin_setcp', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('142', 'admin_sethp', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('143', 'admin_setmp', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('144', 'admin_setchar_cp', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('145', 'admin_setchar_hp', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('146', 'admin_setchar_mp', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('147', 'admin_edit_npc', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('148', 'admin_save_npc', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('149', 'admin_show_droplist', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('150', 'admin_edit_drop', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('151', 'admin_add_drop', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('152', 'admin_del_drop', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('153', 'admin_showShop', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('154', 'admin_showShopList', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('155', 'admin_addShopItem', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('156', 'admin_delShopItem', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('157', 'admin_box_access', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('158', 'admin_editShopItem', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('159', 'admin_close_window', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('160', 'admin_start_monitor_char', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('161', 'admin_stop_monitor_char', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('162', 'admin_block_char_packet', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('163', 'admin_restore_char_packet', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('164', 'admin_invis', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('165', 'admin_invisible', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('166', 'admin_vis', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('167', 'admin_visible', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('168', 'admin_invis_menu', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('169', 'admin_earthquake', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('170', 'admin_earthquake_menu', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('171', 'admin_bighead', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('172', 'admin_shrinkhead', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('173', 'admin_gmspeed', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('174', 'admin_gmspeed_menu', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('175', 'admin_unpara_all', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('176', 'admin_para_all', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('177', 'admin_unpara', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('178', 'admin_para', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('179', 'admin_unpara_all_menu', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('180', 'admin_para_all_menu', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('181', 'admin_unpara_menu', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('182', 'admin_para_menu', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('183', 'admin_polyself', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('184', 'admin_unpolyself', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('185', 'admin_polyself_menu', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('186', 'admin_unpolyself_menu', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('187', 'admin_clearteams', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('188', 'admin_setteam_close', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('189', 'admin_setteam', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('190', 'admin_social', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('191', 'admin_effect', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('192', 'admin_social_menu', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('193', 'admin_effect_menu', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('194', 'admin_abnormal', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('195', 'admin_abnormal_menu', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('196', 'admin_play_sounds', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('197', 'admin_play_sound', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('198', 'admin_shrinkhead', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('199', 'admin_atmosphere', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('200', 'admin_atmosphere_menu', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('201', 'admin_npc_say', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('202', 'admin_debuff', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('203', 'admin_seteh', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('204', 'admin_setec', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('205', 'admin_seteg', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('206', 'admin_setel', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('207', 'admin_seteb', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('208', 'admin_setew', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('209', 'admin_setes', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('210', 'admin_setle', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('211', 'admin_setre', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('212', 'admin_setlf', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('213', 'admin_setrf', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('214', 'admin_seten', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('215', 'admin_setun', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('216', 'admin_setba', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('217', 'admin_enchant', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('218', 'admin_event', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('219', 'admin_event_new', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('220', 'admin_event_choose', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('221', 'admin_event_store', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('222', 'admin_event_set', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('223', 'admin_event_change_teams_number', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('224', 'admin_event_announce', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('225', 'admin_event_panel', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('226', 'admin_event_control_begin', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('227', 'admin_event_control_teleport', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('228', 'admin_add', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('229', 'admin_event_see', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('230', 'admin_event_del', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('231', 'admin_delete_buffer', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('232', 'admin_event_control_sit', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('233', 'admin_event_name', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('234', 'admin_event_control_kill', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('235', 'admin_event_control_res', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('236', 'admin_event_control_poly', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('237', 'admin_event_control_unpoly', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('238', 'admin_event_control_prize', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('239', 'admin_event_control_chatban', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('240', 'admin_event_control_finish', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('241', 'admin_add_exp_sp_to_character', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('242', 'admin_add_exp_sp', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('243', 'admin_remove_exp_sp', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('244', 'admin_fight_calculator', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('245', 'admin_fight_calculator_show', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('246', 'admin_fcs', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('247', 'admin_fortsiege', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('248', 'admin_add_fortattacker', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('249', 'admin_add_fortdefender', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('250', 'admin_add_fortguard', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('251', 'admin_list_fortsiege_clans', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('252', 'admin_clear_fortsiege_list', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('253', 'admin_move_fortdefenders', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('254', 'admin_spawn_fortdoors', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('255', 'admin_endfortsiege', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('256', 'admin_startfortsiege', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('257', 'admin_setfort', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('258', 'admin_removefort', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('259', 'admin_geo_z', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('260', 'admin_geo_type', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('261', 'admin_geo_nswe', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('262', 'admin_geo_los', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('263', 'admin_geo_position', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('264', 'admin_geo_bug', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('265', 'admin_geo_load', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('266', 'admin_geo_unload', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('267', 'admin_gm', '1');
INSERT INTO `character_admin_command_access_right` VALUES ('268', 'admin_gmchat', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('269', 'admin_snoop', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('270', 'admin_gmchat_menu', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('271', 'admin_heal', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('272', 'admin_help', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('273', 'admin_sethero', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('274', 'admin_invul', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('275', 'admin_setinvul', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('276', 'admin_kick', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('277', 'admin_kick_non_gm', '1');
INSERT INTO `character_admin_command_access_right` VALUES ('278', 'admin_kill', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('279', 'admin_kill_monster', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('280', 'admin_add_level', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('281', 'admin_set_level', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('282', 'admin_server_gm_only', '1');
INSERT INTO `character_admin_command_access_right` VALUES ('283', 'admin_server_all', '1');
INSERT INTO `character_admin_command_access_right` VALUES ('284', 'admin_server_max_player', '1');
INSERT INTO `character_admin_command_access_right` VALUES ('285', 'admin_server_list_clock', '1');
INSERT INTO `character_admin_command_access_right` VALUES ('286', 'admin_server_login', '1');
INSERT INTO `character_admin_command_access_right` VALUES ('287', 'admin_mammon_find', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('288', 'admin_mammon_respawn', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('289', 'admin_list_spawns', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('290', 'admin_msg', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('291', 'admin_manor', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('292', 'admin_manor_reset', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('293', 'admin_manor_save', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('294', 'admin_manor_disable', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('295', 'admin_masskill', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('296', 'admin_massress', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('297', 'admin_recallclan', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('298', 'admin_recallparty', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('299', 'admin_recallally', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('300', 'admin_char_manage', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('301', 'admin_teleport_character_to_menu', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('302', 'admin_recall_char_menu', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('303', 'admin_recall_party_menu', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('304', 'admin_recall_clan_menu', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('305', 'admin_goto_char_menu', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('306', 'admin_kick_menu', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('307', 'admin_kill_menu', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('308', 'admin_ban_menu', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('309', 'admin_unban_menu', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('310', 'admin_mobmenu', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('311', 'admin_mobgroup_list', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('312', 'admin_mobgroup_create', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('313', 'admin_mobgroup_remove', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('314', 'admin_mobgroup_delete', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('315', 'admin_mobgroup_spawn', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('316', 'admin_mobgroup_unspawn', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('317', 'admin_mobgroup_kill', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('318', 'admin_mobgroup_idle', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('319', 'admin_mobgroup_attack', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('320', 'admin_mobgroup_rnd', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('321', 'admin_mobgroup_return', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('322', 'admin_mobgroup_follow', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('323', 'admin_mobgroup_casting', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('324', 'admin_mobgroup_nomove', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('325', 'admin_mobgroup_attackgrp', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('326', 'admin_mobgroup_invul', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('327', 'admin_mobinst', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('328', 'admin_mons', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('329', 'admin_setnoble', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('330', 'admin_view_petitions', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('331', 'admin_view_petition', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('332', 'admin_accept_petition', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('333', 'admin_reject_petition', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('334', 'admin_reset_petitions', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('335', 'admin_forge', '1');
INSERT INTO `character_admin_command_access_right` VALUES ('336', 'admin_forge2', '1');
INSERT INTO `character_admin_command_access_right` VALUES ('337', 'admin_forge3', '1');
INSERT INTO `character_admin_command_access_right` VALUES ('338', 'admin_pledge', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('339', 'admin_polymorph', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('340', 'admin_unpolymorph', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('341', 'admin_polymorph_menu', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('342', 'admin_unpolymorph_menu', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('343', 'admin_quest_reload', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('344', 'admin_reload', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('345', 'admin_restore', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('346', 'admin_repair', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('347', 'admin_res', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('348', 'admin_res_monster', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('349', 'admin_ride_wyvern', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('350', 'admin_ride_strider', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('351', 'admin_unride_wyvern', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('352', 'admin_unride_strider', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('353', 'admin_unride', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('354', 'admin_load_script', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('355', 'admin_buy', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('356', 'admin_gmshop', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('357', 'admin_server_shutdown', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('358', 'admin_server_restart', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('359', 'admin_server_abort', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('360', 'admin_siege', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('361', 'admin_add_attacker', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('362', 'admin_add_defender', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('363', 'admin_add_guard', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('364', 'admin_list_siege_clans', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('365', 'admin_clear_siege_list', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('366', 'admin_move_defenders', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('367', 'admin_spawn_doors', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('368', 'admin_endsiege', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('369', 'admin_startsiege', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('370', 'admin_setcastle', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('371', 'admin_removecastle', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('372', 'admin_clanhall', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('373', 'admin_clanhallset', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('374', 'admin_clanhalldel', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('375', 'admin_clanhallopendoors', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('376', 'admin_clanhallclosedoors', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('377', 'admin_clanhallteleportself', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('378', 'admin_show_skills', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('379', 'admin_remove_skills', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('380', 'admin_skill_list', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('381', 'admin_skill_index', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('382', 'admin_add_skill', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('383', 'admin_remove_skill', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('384', 'admin_get_skills', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('385', 'admin_reset_skills', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('386', 'admin_give_all_skills', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('387', 'admin_remove_all_skills', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('388', 'admin_add_clan_skill', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('389', 'admin_show_spawns', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('390', 'admin_spawn', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('391', 'admin_spawn_monster', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('392', 'admin_spawn_index', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('393', 'admin_unspawnall', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('394', 'admin_respawnall', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('395', 'admin_spawn_reload', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('396', 'admin_npc_index', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('397', 'admin_spawn_once', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('398', 'admin_show_npcs', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('399', 'admin_teleport_reload', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('400', 'admin_spawnnight', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('401', 'admin_spawnday', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('402', 'admin_target', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('403', 'admin_show_moves', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('404', 'admin_show_moves_other', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('405', 'admin_show_teleport', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('406', 'admin_teleport_to_character', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('407', 'admin_teleportto', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('408', 'admin_move_to', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('409', 'admin_teleport_character', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('410', 'admin_recall', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('411', 'admin_walk', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('412', 'admin_recall_npc', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('413', 'admin_gonorth', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('414', 'admin_gosouth', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('415', 'admin_goeast', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('416', 'admin_gowest', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('417', 'admin_goup', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('418', 'admin_godown', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('419', 'admin_tele', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('420', 'admin_teleto', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('421', 'admin_test', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('422', 'admin_stats', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('423', 'admin_skill_test', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('424', 'admin_st', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('425', 'admin_mp', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('426', 'admin_known', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('427', 'admin_townwar_start', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('428', 'admin_townwar_end', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('429', 'admin_tvt', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('430', 'admin_tvt_name', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('431', 'admin_tvt_desc', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('432', 'admin_tvt_join_loc', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('433', 'admin_tvt_minlvl', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('434', 'admin_tvt_maxlvl', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('435', 'admin_tvt_npc', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('436', 'admin_tvt_npc_pos', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('437', 'admin_tvt_reward', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('438', 'admin_tvt_reward_amount', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('439', 'admin_tvt_team_add', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('440', 'admin_tvt_team_remove', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('441', 'admin_tvt_team_pos', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('442', 'admin_tvt_team_color', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('443', 'admin_tvt_join', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('444', 'admin_tvt_teleport', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('445', 'admin_tvt_start', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('446', 'admin_tvt_abort', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('447', 'admin_tvt_finish', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('448', 'admin_tvt_sit', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('449', 'admin_tvt_dump', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('450', 'admin_tvt_save', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('451', 'admin_tvt_load', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('452', 'admin_tvt_jointime', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('453', 'admin_tvt_eventtime', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('454', 'admin_tvt_autoevent', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('455', 'admin_tvt_minplayers', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('456', 'admin_tvt_maxplayers', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('457', 'admin_tvtkick', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('458', 'admin_unblockip', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('459', 'admin_vip', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('460', 'admin_vip_setteam', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('461', 'admin_vip_randomteam', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('462', 'admin_vip_settime', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('463', 'admin_vip_endnpc', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('464', 'admin_vip_setdelay', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('465', 'admin_vip_joininit', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('466', 'admin_vip_joinnpc', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('467', 'admin_vip_joinlocxyz', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('468', 'admin_vip_setarea', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('469', 'admin_vip_vipreward', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('470', 'admin_vip_viprewardamount', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('471', 'admin_vip_thevipreward', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('472', 'admin_vip_theviprewardamount', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('473', 'admin_vip_notvipreward', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('474', 'admin_vip_notviprewardamount', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('475', 'admin_walker_setmessage', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('476', 'admin_walker_menu', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('477', 'admin_walker_setnpc', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('478', 'admin_walker_setpoint', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('479', 'admin_walker_setmode', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('480', 'admin_walker_addpoint', '3');
INSERT INTO `character_admin_command_access_right` VALUES ('481', 'admin_zone_check', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('482', 'admin_zone_reload', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('483', 'admin_setaio', '2');
INSERT INTO `character_admin_command_access_right` VALUES ('484', 'admin_removeaio', '2');
