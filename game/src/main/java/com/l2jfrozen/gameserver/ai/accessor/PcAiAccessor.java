package com.l2jfrozen.gameserver.ai.accessor;

import com.l2jfrozen.gameserver.ai.L2PlayerAI;
import com.l2jfrozen.gameserver.model.L2Character;
import com.l2jfrozen.gameserver.model.L2Object;
import com.l2jfrozen.gameserver.model.L2Skill;
import com.l2jfrozen.gameserver.model.actor.instance.L2CubicInstance;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.model.actor.stat.PcStat;
import com.l2jfrozen.gameserver.model.entity.event.CTF;
import com.l2jfrozen.gameserver.model.entity.event.DM;
import com.l2jfrozen.gameserver.model.entity.event.TvT;
import com.l2jfrozen.gameserver.network.serverpackets.ActionFailed;

/**
 * User: vadimDidenko
 * Date: 17.12.13
 * Time: 21:59
 */
public abstract class PcAiAccessor extends AIAccessor<PcStat, L2PlayerAI, L2PcInstance> {

    /**
     * Do pickup item.
     *
     * @param object the object
     */
    public void doPickupItem(L2Object object) {
        getActor().doPickupItem(object);
    }

    /**
     * Do interact.
     *
     * @param target the target
     */
    public void doInteract(L2Character target) {
        getActor().doInteract(target);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doAttack(L2Character target) {
        if (L2Character.isInsidePeaceZone(getActor(), target)) {
            getActor().sendPacket(ActionFailed.STATIC_PACKET);
            return;

        }

        // during teleport phase, players cant do any attack
        if ((TvT.is_teleport() && getActor()._inEventTvT) || (CTF.is_teleport() && getActor()._inEventCTF) || (DM.is_teleport() && getActor()._inEventDM)) {
            getActor().sendPacket(ActionFailed.STATIC_PACKET);
            return;
        }

        super.doAttack(target);

        // cancel the recent fake-death protection instantly if the player attacks or casts spells
        getActor().setRecentFakeDeath(false);

        synchronized (getActor().getCubics()) {
            for (L2CubicInstance cubic : getActor().getCubics().values())
                if (cubic.getId() != L2CubicInstance.LIFE_CUBIC) {
                    cubic.doAction(/* target */);
                }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doCast(L2Skill skill) {
        // cancel the recent fake-death protection instantly if the player attacks or casts spells
        getActor().setRecentFakeDeath(false);
        if (skill == null)
            return;

        // Like L2OFF you can use cupid bow skills on peace zone
        // Like L2OFF players can use TARGET_AURA skills on peace zone, all targets will be ignored.
        if (skill.isOffensive() && (L2Character.isInsidePeaceZone(getActor(), getActor().getTarget()) && skill.getTargetType() != L2Skill.SkillTargetType.TARGET_AURA) && (skill.getId() != 3261 && skill.getId() != 3260 && skill.getId() != 3262)) //check limited to active target
        {
            getActor().sendPacket(ActionFailed.STATIC_PACKET);
            return;

        }

        //during teleport phase, players cant do any attack
        if ((TvT.is_teleport() && getActor()._inEventTvT)
                || (CTF.is_teleport() && getActor()._inEventCTF)
                || (DM.is_teleport() && getActor()._inEventDM)) {
            getActor().sendPacket(ActionFailed.STATIC_PACKET);
            return;
        }

        super.doCast(skill);


        if (!skill.isOffensive())
            return;

        switch (skill.getTargetType()) {
            case TARGET_GROUND:
                return;
            default: {
                L2Object mainTarget = skill.getFirstOfTargetList(getActor());
                if (mainTarget == null || !(mainTarget instanceof L2Character))
                    return;

                synchronized (getActor().getCubics()) {

                    for (L2CubicInstance cubic : getActor().getCubics().values())
                        if (cubic != null && cubic.getId() != L2CubicInstance.LIFE_CUBIC) {
                            cubic.doAction();
                        }

                }

                mainTarget = null;
            }
            break;
        }
    }
}
