/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.handler;

import com.l2jfrozen.configuration.GameServerConfig;
import com.l2jfrozen.database.model.game.AccessLevelType;
import com.l2jfrozen.gameserver.datatables.sql.AdminCommandAccessRights;
import com.l2jfrozen.gameserver.handler.admincommandhandlers.*;
import javolution.util.FastMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * This class ...
 *
 * @version $Revision: 1.1.4.5 $ $Date: 2005/03/27 15:30:09 $
 */
public class AdminCommandHandler {
    protected static final Logger LOGGER = LoggerFactory.getLogger(AdminCommandHandler.class.getName());

    private static AdminCommandHandler _instance;

    private FastMap<String, IAdminCommandHandler> _datatable;

    public static AdminCommandHandler getInstance() {
        if (_instance == null) {
            _instance = new AdminCommandHandler();
        }
        return _instance;
    }

    private AdminCommandHandler() {
        _datatable = new FastMap<>();
        registerAdminCommandHandler(new AdminAdmin());
        registerAdminCommandHandler(new AdminInvul());
        registerAdminCommandHandler(new AdminDelete());
        registerAdminCommandHandler(new AdminKill());
        registerAdminCommandHandler(new AdminTarget());
        registerAdminCommandHandler(new AdminShop());
        registerAdminCommandHandler(new AdminCTFEngine());
        registerAdminCommandHandler(new AdminVIPEngine());
        registerAdminCommandHandler(new AdminDMEngine());
        registerAdminCommandHandler(new AdminAnnouncements());
        registerAdminCommandHandler(new AdminCreateItem());
        registerAdminCommandHandler(new AdminHeal());
        registerAdminCommandHandler(new AdminHelpPage());
        registerAdminCommandHandler(new AdminShutdown());
        registerAdminCommandHandler(new AdminSpawn());
        registerAdminCommandHandler(new AdminSkill());
        registerAdminCommandHandler(new AdminScript());
        registerAdminCommandHandler(new AdminExpSp());
        registerAdminCommandHandler(new AdminEventEngine());
        registerAdminCommandHandler(new AdminGmChat());
        registerAdminCommandHandler(new AdminEditChar());
        registerAdminCommandHandler(new AdminGm());
        registerAdminCommandHandler(new AdminTeleport());
        registerAdminCommandHandler(new AdminRepairChar());
        registerAdminCommandHandler(new AdminChangeAccessLevel());
        registerAdminCommandHandler(new AdminChristmas());
        registerAdminCommandHandler(new AdminBan());
        registerAdminCommandHandler(new AdminPolymorph());
        // registerAdminCommandHandler(new AdminBanChat());
        registerAdminCommandHandler(new AdminReload());
        registerAdminCommandHandler(new AdminKick());
        registerAdminCommandHandler(new AdminMonsterRace());
        registerAdminCommandHandler(new AdminEditNpc());
        registerAdminCommandHandler(new AdminFightCalculator());
        registerAdminCommandHandler(new AdminMenu());
        registerAdminCommandHandler(new AdminSiege());
        registerAdminCommandHandler(new AdminFortSiege());
        registerAdminCommandHandler(new AdminPetition());
        registerAdminCommandHandler(new AdminPForge());
        registerAdminCommandHandler(new AdminBBS());
        registerAdminCommandHandler(new AdminEffects());
        registerAdminCommandHandler(new AdminDoorControl());
        registerAdminCommandHandler(new AdminTest());
        registerAdminCommandHandler(new AdminEnchant());
        registerAdminCommandHandler(new AdminMassRecall());
        registerAdminCommandHandler(new AdminMassControl());
        registerAdminCommandHandler(new AdminMobGroup());
        registerAdminCommandHandler(new AdminRes());
        registerAdminCommandHandler(new AdminMammon());
        registerAdminCommandHandler(new AdminUnblockIp());
        registerAdminCommandHandler(new AdminPledge());
        registerAdminCommandHandler(new AdminRideWyvern());
        registerAdminCommandHandler(new AdminLogin());
        registerAdminCommandHandler(new AdminCache());
        registerAdminCommandHandler(new AdminLevel());
        registerAdminCommandHandler(new AdminQuest());
        registerAdminCommandHandler(new AdminZone());
        registerAdminCommandHandler(new AdminCursedWeapons());
        registerAdminCommandHandler(new AdminGeodata());
        registerAdminCommandHandler(new AdminManor());
        registerAdminCommandHandler(new AdminTownWar());
        registerAdminCommandHandler(new AdminTvTEngine());
        registerAdminCommandHandler(new AdminDonator());
        registerAdminCommandHandler(new AdminNoble());
        registerAdminCommandHandler(new AdminBuffs());
        registerAdminCommandHandler(new AdminAio());
        registerAdminCommandHandler(new AdminCharSupervision());
        registerAdminCommandHandler(new AdminWho()); // L2OFF command
        // ATTENTION: adding new command handlers, you have to change the
        // sql file containing the access levels rights

        LOGGER.info("AdminCommandHandler: Loaded " + _datatable.size() + " handlers.");

        if (GameServerConfig.DEBUG) {
            String[] commands = new String[_datatable.keySet().size()];

            commands = _datatable.keySet().toArray(commands);

            Arrays.sort(commands);

            for (String command : commands) {
                if (AdminCommandAccessRights.getInstance().accessRightForCommand(command) == AccessLevelType.BANNED) {
                    LOGGER.info("ATTENTION: admin command " + command + " has not an access right");
                }
            }

        }

    }

    public void registerAdminCommandHandler(IAdminCommandHandler handler) {
        String[] ids = handler.getAdminCommandList();
        for (String element : ids) {
            if (GameServerConfig.DEBUG) {
                LOGGER.info("Adding handler for command " + element);
            }

            if (_datatable.keySet().contains(element)) {
                LOGGER.info("Duplicated command \"" + element + "\" definition in " + handler.getClass().getName() + ".");
            } else {
                _datatable.put(element, handler);
            }
        }
        ids = null;
    }

    public IAdminCommandHandler getAdminCommandHandler(String adminCommand) {
        String command = adminCommand;

        if (adminCommand.contains(" ")) {
            command = adminCommand.substring(0, adminCommand.indexOf(" "));
        }

        if (GameServerConfig.DEBUG) {
            LOGGER.info("getting handler for command: " + command + " -> " + (_datatable.get(command) != null));
        }

        return _datatable.get(command);
    }
}